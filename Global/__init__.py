import json
import types
import time
from os import path
import struct
import threading
from datetime import datetime
import os
import asyncio
import logging

import secrets
import sys
import socket
import selectors

import random
import http

host = '192.168.43.254' # The remote host
port = 6500  # The same port as used by the server

host_ws = "192.168.43.254"
port_ws = 6800
sel = selectors.DefaultSelector()
ID_Server = "2222"


user_postgres = "postgres"
password_postgres = "19972910"
host_postgres = "127.0.0.1"
port_postgres = "5432"
database_postgres = "db_server_iotkaran"



STATE = {"value": 0}

USERS = []
list_close_ws = []

web_socket_obj = asyncio.get_event_loop()

max_connection = 3
messages = []

logger = logging.getLogger('websockets')
logger.setLevel(logging.DEBUG)
logger.addHandler(logging.StreamHandler())
logging.basicConfig()


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
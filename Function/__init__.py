from Global import *


class Fun:

    def start_connections(connid):

        server_addr = (host, port)
        print("starting connection", connid, "to", server_addr)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        Fun.set_keepalive_linux(sock)
        # Setting this socket option avoids the error Address already in use
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        sock.setblocking(False)

        sock.connect_ex(server_addr)

        events = selectors.EVENT_READ | selectors.EVENT_WRITE
        data = types.SimpleNamespace(connid=connid, outb=b'')
        sel.register(sock, events, data=data)
        return sock

    def set_keepalive_linux(sock, after_idle_sec=1, interval_sec=3, max_fails=5):
        """Set TCP keepalive on an open socket.

        It activates after 1 second (after_idle_sec) of idleness,
        then sends a keepalive ping once every 3 seconds (interval_sec),
        and closes the connection after 5 failed ping (max_fails), or 15 seconds
        """
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_KEEPALIVE, 1)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPIDLE, after_idle_sec)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPINTVL, interval_sec)
        sock.setsockopt(socket.IPPROTO_TCP, socket.TCP_KEEPCNT, max_fails)

    def json_From(data):
        try:
            data_json = json.loads(data)
            tuple = ()

            Value_From = data_json["From"]

            Value_From_Type = Value_From["Type"]
            Value_Form_Token = Value_From["Token"]
            Value_Form_Rule = Value_From["Rule"]

            if Value_From_Type != '' and Value_Form_Token != '' and Value_Form_Rule != '':
                tuple = (Value_From_Type, Value_Form_Token, Value_Form_Rule)

            else:
                tuple = None

            print("from ==>>", tuple)

            if tuple is None:
                return None
            else:
                return tuple

        except ValueError as e:
            print("not jason format")
            return None
        except KeyError as error:
            print(error)
            print("json form key is not exist")
            return None




    def json_To(data):
        try:
            data_json = json.loads(data)
            tuple = ()

            Value_To = data_json["To"]
            Value_To_Type = Value_To["Type"]
            Value_To_Token = Value_To["Token"]
            Value_To_Rule = Value_To["Rule"]


            if Value_To_Type != '' and Value_To_Token != '' and Value_To_Rule != '':

                tuple = (Value_To_Type, Value_To_Token, Value_To_Rule)
            else:
                tuple = None


            print("to ==>>", tuple)
            if tuple is None:
                return None
            else:
                return tuple
        except ValueError as e:
            print("not jason format")
            return None
        except KeyError as error:
            print(error)
            print("json to key is not exist")
            return None

    def json_Time(data):
        try:
            data_json = json.loads(data)
            Value_Time = data_json["Time"]
            if Value_Time != '':
                tuple = (Value_Time)
            else:
                tuple = None

            print("time ==>>", tuple)

            if tuple is None:
                return None
            else:
                return tuple
        except ValueError as e:
            print("not jason format")
        except KeyError as error:
            print(error)
            print("json time key is not exist")
            return None

    def json_Info(data):
        try:
            data_json = json.loads(data)
            tuple = ()
            Value_Info = data_json["Info"]


            Value_Info_Type = Value_Info["Type"]
            Value_Info_Action = Value_Info["Action"]
            Value_Info_Command = Value_Info["Command"]
            Value_Info_Data = Value_Info["Data"]


            # not check data becuse for device command 5 is empty
            if Value_Info_Type != '' and Value_Info_Action != '' and Value_Info_Command != '':

                #this command keep alive thhats why  data is emptyyyyyyyyyyyyy
                tuple = (Value_Info_Type, Value_Info_Action, Value_Info_Command,Value_Info_Data)

            else:
                tuple = None

            print("info ==>> ", tuple)
            if tuple is None:
                return None
            else:
                return tuple

        except ValueError as e:
            print("not jason format")
            return None
        except KeyError as error:
            print(error)
            print("json Info key is not exist")
            return None

    def register(websocket, connid):
        print("register socket")
        sock = Fun.start_connections(connid)
        dir = {"Token": connid, "ws": websocket, "Sock":sock}

        USERS.append(dir)
        print("user", USERS)


    def Login_ws(websocket):
        for tokin in USERS:
            if tokin["ws"] == websocket:
                return True
        return False

    def unregister_ws(websocket):
        print("befor delete USERS", USERS)
        for i in USERS:

            if i["ws"] == websocket:
                dir = {"Token": i["Token"], "ws": i["ws"], "Sock": i["Sock"]}
                USERS.remove(dir)

                i["Sock"].close()
                sel.unregister(i["Sock"])
                list_close_ws.append(i["ws"])

                #remove messeg
                for l in messages:
                    if l["Token"] == i["Token"]:
                        dir = {"Token": l["Token"], "message": l["message"]}
                        messages.remove(dir)


        print("after delete USERS", USERS)

    def unregister_Token(token):
        print("befor delete USERS", USERS)
        for i in USERS:

            if i["Token"] == token:
                dir = {"Token": i["Token"], "ws": i["ws"], "Sock": i["Sock"]}
                USERS.remove(dir)
                i["Sock"].close()
                sel.unregister(i["Sock"])
                list_close_ws.append(i["ws"])


                #remove messeg
                for l in messages:
                    if l["Token"] == i["Token"]:
                        dir = {"Token": l["Token"], "message": l["message"]}
                        messages.remove(dir)

        print("after delete USERS", USERS)

    def unregister_sock(sock):
        print("befor delete USERS", USERS)
        for i in USERS:

            if i["Sock"] == sock:
                dir = {"Token": i["Token"], "ws": i["ws"], "Sock": i["Sock"]}
                USERS.remove(dir)

                i["Sock"].close()
                sel.unregister(i["Sock"])
                list_close_ws.append(i["ws"])

                #remove messeg
                for l in messages:
                    if l["Token"] == i["Token"]:
                        dir = {"Token": l["Token"], "message": l["message"]}
                        messages.remove(dir)

        print("after delete USERS", USERS)





from Global import *
from Function import Fun


def Even(recive, sock, data):
    print(f"{bcolors.WARNING}splite :%s{bcolors.ENDC}" % (recive))
    # check recive format json

    json_info = Fun.json_Info(recive)
    json_to = Fun.json_To(recive)

    if json_to is not None and json_info is not None:
        info_type = json_info[0]
        info_action = json_info[1]
        info_command = json_info[2]
        info_data = json_info[3]


        # check data to
        to_type = json_to[0]
        to_token = json_to[1]
        to_Rule = json_to[2]

        if info_command == 1:
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"

        # command 2 command to device
        elif info_command == 2:
            print("command 2 ")
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"

            # command 2 command to device
        elif info_command == 3:
            print("command 3 ")
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"



        # command 5 keep alive
        elif info_command == 5:
            print("command 5 ")

            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    user["Token"] = to_token
                    data.connid = to_token
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"



            # command 9 room
        elif info_command == 9:
            print("command 9 ")
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"

            # command 10 room
        elif info_command == 10:
            print("command 10 ")
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"


            # command 18 connection
        elif info_command == 18:
            print("command 18 ")
            flag = False
            for user in USERS:
                if user["Sock"] == to_token:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"




            # command 21 connection
        elif info_command == 21:
            print("command 21 ")
            flag = False
            for user in USERS:
                if user["Sock"] == sock:
                    asyncio.run(user["ws"].send(recive))
                    flag = True
            if flag is True:
                return "not answer"
            else:
                return "close"


        else:
            print("protocol from None ")
            return "close"


    else:
        print("recive not json format ")
        return "close"



def send_connections(connid, message):
    dir = {"Token": connid, "message": message }
    messages.append(dir)





def service_connection(key, mask):
    sock = key.fileobj
    data = key.data
    try:

        if mask & selectors.EVENT_READ:
            recv_data = sock.recv(10000)  # Should be ready to read
            print(f"{bcolors.WARNING}received :%s from connection  %s {bcolors.ENDC}" % (recv_data, data.connid))
            if recv_data:
                recives = recv_data.decode("utf-8")
                recive = recives.split('\n')
                for i in range(0, len(recive) - 1):

                    send_data = Even(recive[i], sock, data)
                    # not any awnser to this connetion (Bridge mode)
                    if send_data == "not answer":
                        print("not Awnser to device")


                    # send close to connection close
                    elif send_data == "close":
                        print(f"{bcolors.OKBLUE}closing connection to :%s{bcolors.ENDC}" %(str(sock)))
                        Fun.unregister_sock(sock)


            else:
                print(f"{bcolors.OKBLUE}closing connection to :%s{bcolors.ENDC}" % (str(sock)))
                Fun.unregister_sock(sock)



        if mask & selectors.EVENT_WRITE:
            # print(data.connid)
            # print(messages)
            if len(messages) != 0:

                if messages[0]["Token"] == data.connid:
                    messeg = messages.pop(0)
                    data.outb = messeg["message"]


            if data.outb:
                print("sending", repr(data.outb), "to connection", data.connid)
                sent = sock.send(data.outb)  # Should be ready to write
                data.outb = data.outb[sent:]




    except IOError as error:
        print(f"{bcolors.FAIL}Exeption:%s{bcolors.ENDC}" % (error))
        Fun.unregister_sock(sock)


    except UnicodeDecodeError as error:
        print(f"{bcolors.FAIL}Exeption:%s{bcolors.ENDC}" % (error))
        Fun.unregister_sock(sock)



    except AttributeError as error:
        print(f"{bcolors.FAIL}Exeption:%s{bcolors.ENDC}" % (error))
        Fun.unregister_sock(sock)


    except ValueError as error:
        print(f"{bcolors.FAIL}Exeption:%s{bcolors.ENDC}" % (error))
        Fun.unregister_sock(sock)


    except Exception as error:
        print(f"{bcolors.FAIL}Exeption:%s{bcolors.ENDC}" % (error))
        Fun.unregister_sock(sock)

def init_socket():
    try:
        server_addr = (host, port)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        Fun.set_keepalive_linux(sock)
        # Setting this socket option avoids the error Address already in use
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        sock.setblocking(False)

        sock.connect_ex(server_addr)
        data = types.SimpleNamespace(connid='1', outb=b'')
        # this parent all object
        sel.register(sock, selectors.EVENT_WRITE, data=data)

        while True:
            events = sel.select(timeout=None)
            # print(events)
            if events:
                for key, mask in events:
                    # print(mask)
                    service_connection(key, mask)
            # Check for a socket being monitored to continue.
            if not sel.get_map():
                break


    except IOError as error:
        print(error)
        pass


    except UnicodeDecodeError as error:
        print(error)
        pass


    except AttributeError as error:
        print(error)
        pass


    except ValueError as error:
        print(error)
        pass





